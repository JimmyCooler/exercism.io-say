const numDictionary = {
    0: 'zero',
    1: 'one',
    2: 'two',
    3: 'three',
    4: 'four',
    5: 'five',
    6: 'six',
    7: 'seven',
    8: 'eight',
    9: 'nine',
    10: 'ten',
    11: 'eleven',
    12: 'twelve',
    13: 'thirteen',
    14: 'fourteen',
    15: 'fifteen',
    16: 'sixteen',
    17: 'seventeen',
    18: 'eighteen',
    19: 'nineteen',
    20: 'twenty',
    30: 'thirty',
    40: 'forty',
    50: 'fifty',
    60: 'sixty',
    70: 'seventy',
    80: 'eighty',
    90: 'ninety',
    100: 'hundred',
};

const numLevel = ['thousand', 'million', 'billion'];


function inEnglish(num) {

    if(num<0 || num>999999999999)
    {
        throw new Error('Number must be between 0 and 999,999,999,999.');
    }

    if(num === 0){
        return 'zero';
    }

    let numArr = [];
    let maxLevel = Math.floor(Math.log10(num)/3);
    let currentLevel = 0;
    while(currentLevel <= maxLevel)
    {
        const processThreeDigitArr = processFirstThreeDigit(num, currentLevel);
        if(currentLevel>0 && processThreeDigitArr.length>0)
        {
            processThreeDigitArr.push(numLevel[currentLevel-1]);
        }

        numArr = processThreeDigitArr.concat(numArr);

        currentLevel = currentLevel + 1;
        num = Math.floor(num/1000)
    }

    return showNumStr(numArr);

}


function processFirstThreeDigit(num)
{
    let numArr = [];
    let firstDigitStr = '';

    const firstDigit = num % 10;

    const secondDigit = num - (num % 10) - Math.floor(num/100)*100;
    let secondDigitStr = '';

    const secondFirstDigit = secondDigit + firstDigit;

    if(secondFirstDigit < 20)
    {
        firstDigitStr = numDictionary[secondFirstDigit];
        if(secondFirstDigit !== 0)
        {
            numArr.unshift(firstDigitStr);
        }


    }

    if(num < 20)
    {
        return numArr;
    }

    if(secondDigit !== 0 && secondDigit >= 20)
    {
        secondDigitStr = numDictionary[secondDigit];
        numArr = [];
        if(firstDigit>0)
        {
            firstDigitStr = numDictionary[firstDigit];
            const firstSecondDigitStr = secondDigitStr + '-' + firstDigitStr;
            numArr.unshift(firstSecondDigitStr);
        }else{
            numArr.unshift(secondDigitStr);
        }

    }

    if(num < 100)
    {
        return numArr;
    }

    let thirdDigit = Math.floor(num/100);
    let thirdDigitStr = '';

    if(thirdDigit > 10)
    {
        thirdDigit = thirdDigit % 10;
    }


    if(thirdDigit !== 0 && thirdDigit < 10)
    {
        thirdDigitStr = numDictionary[thirdDigit] + ' ' + 'hundred';
        numArr.unshift(thirdDigitStr);
    }

    return numArr;
}


function showNumStr(numArr)
{
    return numArr.join(' ')
}

module.exports = {
    inEnglish
};